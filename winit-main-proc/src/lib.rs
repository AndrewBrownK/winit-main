//! Procedural macros for `winit-main`.
//!
//! Re-exported by `winit-main` if `winit-main` is compiled with the `proc`
/// feature.

use proc_macro::TokenStream;
use syn::{
    parse_macro_input,
    parse_quote,
};
use quote::quote;


/// Proc-macro around a "simulated main thread" function, which hijacks itself
/// with `winit_main::run` and spawns the function body in a new thread with
/// callbacks to communcate with the main thread. 
///
/// Input:
/// ```rust,compile_fail
/// #[winit_main::main]
/// fn main(event_loop: EventLoopHandle, events: EventReceiver) {
///     // stuff goes here
/// }
/// ```
///
/// Output:
/// ```rust,compile_fail
/// fn main() -> ! {
///     fn _inner(event_loop: EventLoopHandle, events: EventReceiver) {
///         // stuff goes here
///     }
///     ::winit_main::run(_inner)
/// }
/// ```
#[proc_macro_attribute]
pub fn main(_attr: TokenStream, item: TokenStream) -> TokenStream {
    let item = parse_macro_input!(item as syn::ItemFn);

    let mut inner_fn = item.clone();
    inner_fn.sig.ident = parse_quote! { _inner };

    let outer_fn = syn::ItemFn {
        attrs: item.attrs.clone(),
        vis: item.vis.clone(),
        sig: syn::Signature {
            unsafety: item.sig.unsafety.clone(),
            ident: item.sig.ident.clone(),
            generics: item.sig.generics.clone(),

            constness: None,
            asyncness: None,
            abi: None,
            variadic: None,
            output: parse_quote! { -> ! },

            fn_token: Default::default(),
            paren_token: Default::default(),
            inputs: Default::default(),
        },
        block: Box::new(parse_quote! {
            {
                #inner_fn
                ::winit_main::run(_inner)
            }
        }),
    };

    (quote! { #outer_fn }).into()
}
